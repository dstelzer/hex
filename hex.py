# Utility class for manipulating hexes, using Generalized Balanced Ternary (GBT_2)

#  5 1
# 4 0 3
#  6 2

from itertools import combinations
from functools import lru_cache
from collections import Counter

class Hex:
	
	# Basic definition of the class itself: almost nothing except a wrapper around an int
	
	_debug = True
	__slots__ = ('data',)
	
	def __init__(self, dat_=0):
		if isinstance(dat_, str): # Parse a string into a septimal number
			self.data = int(dat_, 7)
		else: # Otherwise just use standard int conversion
			self.data = int(dat_)
	
	# Conversion operators
	
	def __hash__(self):
		return self.data
	
	def __int__(self):
		return self.data
	
	def __index__(self):
		return self.data
	
	# Comparison operators
	
	def __eq__(self, other):
		return self.data == int(other)
	
	def __req__(self, other):
		return self.data == int(other)
	
	def __ne__(self, other):
		return not (self == other)
	
	def __rne__(self, other):
		return not (self == other)
	
	def __bool__(self): # Does this represent a nonzero point?
		return self.data != 0
	
	def __lt__(self, other): # Make a total ordering so they can be sorted reliably
		return self.data < int(other)
	
	def __gt__(self, other):
		return self.data > int(other)
	
	def __le__(self, other):
		return self.data <= int(other)
	
	def __ge__(self, other):
		return self.data >= int(other)
	
	def __rge__(self, other):
		return self.data < int(other)
	
	def __rle__(self, other):
		return self.data > int(other)
	
	def __rgt__(self, other):
		return self.data <= int(other)
	
	def __rlt__(self, other):
		return self.data >= int(other)
	
	# Static methods for septimal arithmetic
	
	@classmethod
	def negate_single(cls, a):
		if cls._debug and a > 6: raise ValueError('negate_single only handles single-sept inputs')
		if a == 0: return 0
		return 7 - a
	
	@classmethod
	def negate_many(cls, a):
		out = 0
		mul = 1
		while a:
			out += mul * cls.negate_single(a%7)
			a //= 7
			mul *= 7
		return cls(out)
	
	carrytable = (
		(0, 0, 0, 0, 0, 0, 0),
		(0, 1, 0, 3, 0, 1, 0),
		(0, 0, 2, 2, 0, 0, 6),
		(0, 3, 2, 3, 0, 0, 0),
		(0, 0, 0, 0, 4, 5, 4),
		(0, 1, 0, 0, 5, 5, 0),
		(0, 0, 6, 0, 4, 0, 6)
	)
	
	@classmethod
	def sum_single(cls, a, b):
		if cls._debug and (a > 6 or b > 6): raise ValueError('sum_single only handles single-sept inputs')
		remain = (a + b) % 7 # The remainder sept is easy to calculate
		carry = cls.carrytable[a][b] # Carrying is a bit weird however, due to the specifics of the system - hard-coded table is the best way
		return carry * 7 + remain
	
	@classmethod
	def sum_many(cls, a, b):
		out = 0
		mul = 1
		while a or b:
			out += mul * cls.sum_single(a%7, b%7)
			a //= 7
			b //= 7
			mul *= 7
		return cls(out)
	
	@classmethod
	def product_single(cls, a, b): # Thankfully, the product of two septs is always a sept, with no carry
		if cls._debug and (a > 6 or b > 6): raise ValueError('sum_single only handles single-sept inputs')
		return (a * b) % 7 # Nice and simple, yay!
	
	@classmethod
	def product_single_many(cls, a, b): # A is a single sept, B doesn't have to be
		out = 0
		mul = 1
		while b:
			out += mul * cls.product_single(a, b%7)
			b //= 7
			mul *= 7
		return out
	
	@classmethod
	def product_many(cls, a, b):
		out = 0
		mul = 1
		while a:
			out += mul * cls.product_single_many(a%7, b)
			a //= 7
			mul *= 7
		return cls(out)
	
	# Operator methods to call these
	
	def __pos__(self): # A là C++, use the unary plus operator to decay into an integer
		return self.data
	
	def __neg__(self): # Additive inverse
		return Hex.negate_many(self.data)
	
	def __add__(self, other): # Vector addition
		return Hex.sum_many(int(self), int(other))
	
	def __radd__(self, other):
		return self + other
	
	def __sub__(self, other): # Inverted vector addition
		return self + (-other)
		
	def __rsub__(self, other):
		return other + (-self)
	
	def __matmul__(self, other): # Vector rotation
		return Hex.product_many(int(self), int(other))
	
	def __rmatmul__(self, other):
		return Hex.product_many(int(other), int(self))
	
	def __mul__(self, other): # Scaling
		out = Hex()
		for _ in range(other):
			out += self
		return out
	
	def __rmul__(self, other):
		return self * other
	
	# Convenience methods
	
	def __str__(self): # Write out data in septimal
		a = self.data
		if not a: return '0'
		out = []
		while a:
			out.append(str(a%7))
			a //= 7
		return ''.join(reversed(out))
	
	def __repr__(self):
		return "Hex('{}')".format(str(self))
	
	# Simplify to steps - inefficient and slow. Is there a better way? Use caching to help
	
	@staticmethod
	def separate_first(a): # The first sept digit from a number, and its position
		last = 0
		mul = 1
		while a:
			last = a
			a //= 7
			mul *= 7
		return last, mul // 7
	
	tens = (0, 5, 3, 1, 6, 4, 2) # a + a + tens[a] = a0
	
	@classmethod
	def divten(cls, a): # Return three numbers x, y, z such that x + y + z = a0
		return a, a, cls.tens[a]
	
	@classmethod
	def divten_ext(cls, a): # Same as above but scaled
		if not a: return () # 0 -> empty tuple
		sept, high = cls.separate_first(a)
		scale = high // 7
		return sept*scale, sept*scale, cls.tens[sept]*scale
	
	@classmethod
	def simplify(cls, path): # Shorten a long list of steps into a minimal path - NOTE: ARGUMENT IS MUTATED
		while sum(1 for k, v in path.items() if v) > 2: # Count how many different types of step appear in this path
			for x, y in combinations((1, 2, 3, 4, 5, 6), 2): # Look at each pair of elements
				if cls.carrytable[x][y] == 0: # The sum of these two variables has no carry, thus it's a single step
					cut = min(path[x], path[y]) # How many times do they both appear?
					path[x] -= cut
					path[y] -= cut
					path[(x+y)%7] += cut # Remove that many instances from each summand, and add them to the sum
			del path[0]
		# May have to repeat all of this because new steps added in process
	
	@staticmethod
	@lru_cache(maxsize=256) # We'll want to memoize this since it's a somewhat expensive algorithm
	def makesteps(end, single=False, tabs=0): # Return a series of single-sept steps which take you from the origin to the endpoint		
		if not end: return Counter()
		if end < 7: return Counter({end:1}) # Nothing more to do for this one - base case
		
		if single: # We have a single sept to work with, with zeroes after it
			steps = Hex.divten_ext(end)
		else:
			# This hasn't already been simplified, we'll need to do that to take advantage of divide-by-seven formula
			# Construct the initial list of steps from our endpoint
			steps = []
			a = end
			mul = 1
			while a: # Break 254 into 200 50 4
				if a%7: steps.append(mul*(a%7))
				mul *= 7
				a //= 7
		
		# Now do the main calculation recursively like a tree
		path = Counter()
		for s in steps:
			path.update(Hex.makesteps(s, True, tabs+1)) # Make sure not to mutate any of the makesteps() return values, since they're also referenced in the cache!
		
		# Now simplify this
		Hex.simplify(path)
		
		# And send it back to our caller
		return path
	
	@staticmethod
	@lru_cache(maxsize=128) # Similarly will happen a lot, want to make sure makesteps is not called more than needed
	def countsteps(end):
		return sum(Hex.makesteps(end).values())
	
	@property
	def steps(self):
		return Hex.makesteps(self.data)
	
	@property
	def magn(self):
		return Hex.countsteps(self.data)
	
	# Operator wrapper
	
	def __abs__(self):
		return self.magn

class direction:
	ne = Hex(1)
	e = Hex(3)
	se = Hex(2)
	
	nw = Hex(5)
	w = Hex(4)
	sw = Hex(6)
	
	directions = ne, e, se, sw, w, nw
	half = ne, e, se
