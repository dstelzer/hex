import csv
from tqdm import trange
from itertools import combinations

from hex import Hex

encode = ['_', 'a', 'b', 'c', 'd', 'e', 'f']

with open('table.csv', 'w', encoding='utf-8', newline='') as f:
	write = csv.writer(f)
	write.writerow(('dec', 'sept', 'dist', 'path'))
	
	for i in trange(7**5):
		h = Hex(i)
		p = ''.join(encode[n] for n in h.steps.elements())
		
		write.writerow((i, h, abs(h), p))
